// Adafruit IO Temperature & Humidity Example
// Tutorial Link: https://learn.adafruit.com/adafruit-io-basics-temperature-and-humidity
//
// Adafruit invests time and resources providing this open source code.
// Please support Adafruit and open source hardware by purchasing
// products from Adafruit!
//
// Written by Todd Treece for Adafruit Industries
// Copyright (c) 2016-2017 Adafruit Industries
// Licensed under the MIT license.
//
// All text above must be included in any redistribution.

/************************** Configuration ***********************************/

// edit the config.h tab and enter your Adafruit IO credentials
// and any additional configuration needed for WiFi, cellular,
// or ethernet clients.
#include "config.h"

/************************ Example Starts Here *******************************/
#include <Adafruit_Sensor.h>

// set up the 'temperature' and 'humidity' feeds
AdafruitIO_Feed *temperature = io.feed("temperature");
AdafruitIO_Feed *humidity = io.feed("humidity");
AdafruitIO_Feed *iaq = io.feed("iaq");
AdafruitIO_Feed *voc = io.feed("voc");
AdafruitIO_Feed *dust = io.feed("dust");

void setup() 
{

  // start the serial connection
  Serial.begin(115200);

  // wait for serial monitor to open
  while(! Serial);

  // connect to io.adafruit.com
  Serial.print("Connecting to Adafruit IO");
  io.connect();

  // wait for a connection
  while(io.status() < AIO_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  // we are connected
  Serial.println();
  Serial.println(io.statusText());
}

float temp;
float hum;
float iaq_v;
float voc_v;
float dust_v;

void loop() {

  // io.run(); is required for all sketches.
  // it should always be present at the top of your loop
  // function. it keeps the client connected to
  // io.adafruit.com, and processes any incoming data.
  io.run();

  temp = rand() % 100;
  hum = rand() % 100;
  iaq_v = rand() % 100;
  voc_v = rand() % 100;
  dust_v = rand() % 100;

  Serial.print("Temperatere: ");
  Serial.printf("%02f Degree\n", temp);
  temperature->save(temp);

  
  Serial.print("humidity: ");
  Serial.printf("%02f %\n", hum);
  humidity->save(hum);

  
  Serial.print("IAQ: ");
  Serial.printf("%02f %\n", iaq_v);
  iaq->save(iaq_v);

  
  Serial.print("VOC: ");
  Serial.printf("%02f %\n", voc_v);
  voc->save(voc_v);

  
  Serial.print("DUST: ");
  Serial.printf("%02f %\n", dust_v);
  dust->save(dust_v);


  // wait 5 seconds (5000 milliseconds == 5 seconds)
  delay(60000);

}
